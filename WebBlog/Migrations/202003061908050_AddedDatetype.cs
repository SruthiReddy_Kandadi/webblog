namespace WebBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDatetype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "CreatedOn", c => c.DateTime(nullable: false));
            DropColumn("dbo.Blogs", "DateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blogs", "DateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Blogs", "CreatedOn");
        }
    }
}

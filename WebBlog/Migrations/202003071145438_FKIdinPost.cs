namespace WebBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FKIdinPost : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Blogs", "Author_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Posts", "Blog_Id", "dbo.Blogs");
            DropIndex("dbo.Blogs", new[] { "Author_Id" });
            DropIndex("dbo.Posts", new[] { "Blog_Id" });
            RenameColumn(table: "dbo.Posts", name: "Blog_Id", newName: "BlogId");
            AlterColumn("dbo.Posts", "BlogId", c => c.Int(nullable: false));
            CreateIndex("dbo.Posts", "BlogId");
            AddForeignKey("dbo.Posts", "BlogId", "dbo.Blogs", "Id", cascadeDelete: true);
            DropColumn("dbo.Blogs", "Author_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blogs", "Author_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.Posts", "BlogId", "dbo.Blogs");
            DropIndex("dbo.Posts", new[] { "BlogId" });
            AlterColumn("dbo.Posts", "BlogId", c => c.Int());
            RenameColumn(table: "dbo.Posts", name: "BlogId", newName: "Blog_Id");
            CreateIndex("dbo.Posts", "Blog_Id");
            CreateIndex("dbo.Blogs", "Author_Id");
            AddForeignKey("dbo.Posts", "Blog_Id", "dbo.Blogs", "Id");
            AddForeignKey("dbo.Blogs", "Author_Id", "dbo.AspNetUsers", "Id");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBlog.Models.DB
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [StringLength(10000)]
        public string Content { get; set; }
        [Required]
        [StringLength(1000)]
        public string Tags { get; set; }

        //FK
        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
    }
}
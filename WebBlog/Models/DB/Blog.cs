﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBlog.Models.DB
{
    public class Blog
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM,dd,yyyy}")]
        public DateTime CreatedOn { get; set; }


        ///FK
        //public int UserId { get; set; }
        //public virtual ApplicationUser Author { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

    }
}